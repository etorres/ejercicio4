class TestFraccion(unittest, TestCase):

    def test_suma01(self):
        f1= Fraccion(1, 2)
        f2= Fraccion(1, 2)

        suma=f1.suma(f2)

        self.assertEqual (suma, numerador, 1)
        self.assertEqual (suma, denominador, 1)

    def test_sum02(self):
        f1= Fraccion(1, 2)
        f2= Fraccion(-1, 2)

        suma=f1.suma(f2)

        self.assertEqual (suma, numerador, 1)
        self.assertEqual (suma, denominador, 1)
if __name__ == "__main__":
    unittest.main()