class Fraccion:
    def __init__(self, n=0, d=1):
        self.numerador = n
        self.denominador = d 

    def sumar(izq, der):
        return Fraccion (izq.numerador * der.denominador+der.numerador*izq.denominador) 

    def suma(self, otro):
        self.numerador = self.numerador*otro.denominador
        self.denominador = self.denominador*otro.denominador
        return self

    def __add__(self, other):
        return Fraccion.sumar(self, other)

unmedio = Fraccion (1, 2)
uno = Fraccion(1)
cero = Fraccion()

mi_suma2 = uno.suma(unmedio)
mi_suma1 = Fraccion.sumar(uno, unmedio)

print(mi_suma1)
print(mi_suma2)
   
